# README #


This repository is just to show the code that was used to get the HUD working for the game EscarGO!. There isn't an application included, just the code for the HUD.

If you'd like to contact me, you can E-mail me at: llbrothers@smu.edu! Also, if you're interested in seeing more of my work please check out my portfolio at: www.lauralbrothers.com

Thanks!