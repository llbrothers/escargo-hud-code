class ECGGFxHUD extends GFxMoviePlayer;

var int lastHealthPc, lastAmmoCount;

var GFxObject ammoCurrent_txt, ammoMax_txt;
var GFxObject healthPoint_txt;
var GFxObject redScore_txt, blueScore_txt;
var GFxObject clockTimerSec_txt, clockTimerMin_txt;
var GFxObject healthBar_mc, ammoBar_mc;
var GFxObject redIcerAmmo_mc, redPepperAmmo_mc, blueIcerAmmo_mc, bluePepperAmmo_mc, redHealth_mc, blueHealth_mc;
var GFxObject redPotMask_mc, bluePotMask_mc;
var GFxObject cookingTimer_txt;
var GFxObject waitingScreen_mc, waitingScreen2_mc;
var GFxObject FrenchWaiting_mc, FrenchWaiting2_mc;
var GFxObject chargeBar_mc, chargeOutline_mc;
var GFxObject flagIndicator_mc;
var GFxObject damageIndicator_mc;
var GFxObject reticule_mc;
var GFxObject blueVictory_mc, redVictory_mc;
var GFxObject waitingScreenTimerText_mc;

var GFxObject boxThingy_mc, waitingText_txt;

var PlayerController PlayerOwner;
var int blueScore, redScore;
var int minutes, seconds;
var UTGameReplicationInfo GettingScores;
var float waitTimer;

var float healthFullLimit, healthEmptyLimit;
var float ammoFullLimit, ammoEmptyLimit;
var float ammoBoxShowing, ammoBoxHidden;
var float chargeBoxFull, chargeBoxEmpty;
var float chargeOutlineShowing, chargeOutlineHidden;

var float blueTempBarFull, blueTempBarEmpty, redTempBarFull, redTempBarEmpty;

var float healthBoxShowing, healthBoxHidden;

var float damageTimer;
var float victoryTimer;
var float waitingScreenTimer;

var bool isInEnglish;

function int roundNum(float NumIn)
{
	local int iNum;
	local float fNum;

	fNum = NumIn;
	iNum = int(fNum);
	fNum -= iNum;
	if( fNum >= 0.5f)
	{
		return(iNum + 1);
	}
	else
	{
		return iNum;
	}
}


function float getpc2(float val, float max)
{
	return roundNum((val / max) * 100.0f);
}


function Init2( PlayerController PC)
{
	local ECG_GameState gameState;

	Start();
	Advance(0.f);

	gameState = new class'ECG_GameState'; 
	if (class'Engine'.static.BasicLoadObject(gameState, "state.bin", true, 0))
		isInEnglish = gameState.isInEnglish;

	lastHealthPc = -1337;
	lastAmmoCount = -101;

	ammoCurrent_txt = GetVariableObject("_level0.currentAmmo_txt");
	healthPoint_txt = GetVariableObject("_level0.health_txt");
	redScore_txt = GetVariableObject("_level0.redScore_txt");
	blueScore_txt = GetVariableObject("_level0.blueScore_txt");
	clockTimerMin_txt = GetVariableObject("_level0.Timer.clockMin_txt");
	clockTimerSec_txt = GetVariableObject("_level0.Timer.clockSec_txt");

	healthBar_mc = GetVariableObject("_level0.HealthRet");
	ammoBar_mc = GetVariableObject("_level0.AmmoRet");

	redHealth_mc = GetVariableObject("_level0.RedHealthThing");
	blueHealth_mc = GetVariableObject("_level0.BlueHealthThing");

	redIcerAmmo_mc = GetVariableObject("_level0.RedIcer");
	redPepperAmmo_mc = GetVariableObject("_level0.RedPepper");

	blueIcerAmmo_mc = GetVariableObject("_level0.BlueIcer");
	bluePepperAmmo_mc = GetVariableObject("_level0.BluePepper");

	redPotMask_mc = GetVariableObject("_level0.RedPotMask");
	bluePotMask_mc= GetVariableObject("_level0.BluePotMask");
	cookingTimer_txt = GetVariableObject("_level0.CookingTimer");

	waitingScreen_mc = GetVariableObject("_level0.WaitingScreen");
	waitingScreen2_mc = GetVariableObject("_level0.WaitingScreen2");
	FrenchWaiting_mc = GetVariableObject("_level0.frenchWaiting");
	FrenchWaiting2_mc = GetVariableObject("_level0.FrenchGameplay");

	chargeBar_mc = GetVariableObject("_level0.ChargeMask");
	chargeOutline_mc = GetVariableObject("_level0.ChargeOutlineMask");

	flagIndicator_mc = GetVariableObject("_level0.FlagIndicator");

	damageIndicator_mc = GetVariableObject("_level0.DamageIndicator");
	reticule_mc = GetVariableObject("_level0.ReticuleMovie");

	blueVictory_mc = GetVariableObject("_level0.BlueVictory");
	redVictory_mc = GetVariableObject("_level0.RedVictory");

	waitingScreenTimerText_mc = GetVariableObject("_level0.WaitingTimer");

	boxThingy_mc = GetVariableObject("_level0.whiteBox_mc");
	waitingText_txt = GetVariableObject("_level0.gameStart_txt");

	SetViewScaleMode(SM_ExactFit);
}


function TickHUD(ECGGameReplicationInfo UTGRI, UTPlayerReplicationInfo playerPRI)
{
	local UTPawn UTP;
	local UTWeapon Weapon;
	local PlayerController PC;

	local int i;
	local int m;
	local string stringSeconds;
	local string redCap, blueCap;
	local int redInt, blueInt;
	local float healthBarValue, healthBarX, healthBarY;
	local float ammoBarValue, ammoBarX, ammoBarY;

	local float redIcerX, redIcerY, redPepperX, redPepperY;
	local float blueIcerX, blueIcerY, bluePepperX, bluePepperY;

	local float redPotX, redPotY, bluePotX, bluePotY;

	local float chargeOutlineX, chargeOutlineY, chargeBarX, chargeBarY;

	local float blueTempValue, redTempValue;
	local float chargeBarTempValue;
	local float redHealthX, redHealthY, blueHealthX, blueHealthY;

	local float timerValue;
	local float hitTimer;

	local LinearColor color2;
	local ASColorTransform colorTransform;

	local string waitTimerString;
	local int currentWaitTimer;
	local int numPlayers;

	PC=GetPC();
	UTP=UTPawn(PC.Pawn);
	Weapon=UTWeapon(UTP.Weapon);
	i=Weapon.GetAmmoCount();
	m=Weapon.MaxAmmoCount;
	blueScore = UTGRI.Teams[1].Score;
	redScore = UTGRI.Teams[0].Score;

	if( redScore == 3 || blueScore == 3 )
	{
		if( victoryTimer > 0.0 )
		{
			victoryTimer-=(1.0/60.0);
		}
		else
		{
			if( blueScore == 3 )
				blueVictory_mc.SetVisible(true);
			if( redScore == 3 )
				redVictory_mc.SetVisible(true);

			return;
		}
	}

	flagIndicator_mc.SetVisible(false);
	blueVictory_mc.SetVisible(false);
	redVictory_mc.SetVisible(false);


	if( damageTimer == 0.0 || getpc2(UTP.Health, UTP.HealthMax) == UTP.HealthMax )
		damageIndicator_mc.SetVisible(false);
	else
		damageIndicator_mc.SetVisible(true);

	if( playerPRI.bHasFlag )
		flagIndicator_mc.SetVisible(true);

	redHealth_mc.GetPosition(redHealthX, redHealthY);
	blueHealth_mc.GetPosition(blueHealthX, blueHealthY);

	//Make sure the right health image shows up for the right team.
	if( UTP == none )
	{

	}
	else if( UTP.GetTeamNum() == 0 )
	{
		redHealth_mc.SetPosition(healthBoxShowing, redHealthY);

		if( blueHealthX >= healthBoxShowing )
			blueHealth_mc.SetPosition(healthBoxHidden, blueHealthY);
	}
	else if( UTP.GetTeamNum() == 1 )
	{
		blueHealth_mc.SetPosition(healthBoxShowing, blueHealthY);

		if( redHealthX >= healthBoxShowing )
			redHealth_mc.SetPosition(healthBoxHidden, redHealthY);
	}


	//Updating the Scores
	blueScore_txt.SetString("text", string(blueScore));
	redScore_txt.SetString("text", string(redScore));


	//Updating the Capture Timers
	minutes = UTGRI.RemainingTime / 60;
	seconds = UTGRI.RemainingTime % 60;

	redInt = FCeil(UTGRI.redCookTimer);
	blueInt = FCeil(UTGRI.blueCookTimer);
	redCap = string(redInt);
	blueCap = string(blueInt);

	if( redInt < 10 )
		redCap = "0" $ redCap;

	if( blueInt < 10 )
		blueCap = "0" $ blueCap;

	stringSeconds = string(seconds);
	if( seconds < 10 )
		stringSeconds = "0" $ stringSeconds;


	//Updating the Clock Timer
	clockTimerMin_txt.SetString("text", string(minutes));
	clockTimerSec_txt.SetString("text", stringSeconds);

	if( UTGRI.snailTeamIndex == -1 )
	{
		blueInt = 0.0;
		redInt = 0.0;
	}

	numPlayers = UTGRI.numPlayers;
	if( numPlayers < 6  )
	{
		if( isInEnglish == false )
			waitingText_txt.SetString("text", "En attente de plus de joueurs");
		else
			waitingText_txt.SetString("text", "Waiting For More Players");
	}
	else
	{
		if( isInEnglish == false )
			waitingText_txt.SetString("text", "Les mises en jeu:");
		else
			waitingText_txt.SetString("text", "Game Starts In:");
	
		waitTimer -= (1.0/60.0);
		currentWaitTimer = FCeil(waitTimer);
		waitTimerString = string(currentWaitTimer);
		if( currentWaitTimer < 10 )
			waitTimerString = "0" $ waitTimerString;
	
		if( currentWaitTimer > 0 )
		{
			waitingScreenTimerText_mc.SetString("text", waitTimerString);
			waitingScreenTimerText_mc.SetVisible(true);
		}
		else
		{
			waitingScreenTimerText_mc.SetString("text", "");
			waitingScreenTimerText_mc.SetVisible(false);
		}
	}


	//If you die, set everything to zero.
	if( UTP == none )
	{
		if( isInEnglish == true )
		{
			FrenchWaiting_mc.SetVisible(false);
			FrenchWaiting2_mc.SetVisible(false);
			if( numPlayers == 6 )
			{
				waitingScreenTimer += (1.0/60.0);
				if (waitingScreenTimer >= 7.5)
				{
					waitingScreenTimerText_mc.SetVisible(true);
					waitingScreen2_mc.SetVisible(false);
				}
			}
		}
		else
		{
			waitingScreen2_mc.SetVisible(false);
			waitingScreen_mc.SetVisible(false);
			if( numPlayers == 6 )
			{
				waitingScreenTimer += (1.0/60.0);
				if( waitingScreenTimer >= 7.5 )
				{
					waitingScreenTimerText_mc.SetVisible(true);
					FrenchWaiting2_mc.SetVisible(false);
				}
			}
		}

		if( currentWaitTimer <= 0 )
			waitingScreenTimerText_mc.SetVisible(false);


		lastHealthPc = 0;
		healthPoint_txt.SetString("text", string(lastHealthPc));
		//Updating the Health Bar on the Reticule
		healthBar_mc.GetPosition(healthBarX, healthBarY);
		healthBarValue = (healthFullLimit - healthEmptyLimit) * (lastHealthPc / 100.0) + healthEmptyLimit;
		healthBar_mc.SetPosition(healthBarX, healthBarValue);

		lastAmmoCount = 0;
		ammoCurrent_txt.SetString("text", string(i));
		//Updating the Ammo Bar on the Reticule
		ammoBar_mc.GetPosition(ammoBarX, ammoBarY);
		ammoBarValue = (ammoFullLimit - ammoEmptyLimit) * (lastAmmoCount / float(m)) + ammoEmptyLimit;
		ammoBar_mc.SetPosition(ammoBarX, ammoBarValue);

		return;
	}

	//Remove waiting screen
	waitingScreen_mc.SetVisible(false);
	waitingScreen2_mc.SetVisible(false);
	FrenchWaiting2_mc.SetVisible(false);
	FrenchWaiting_mc.SetVisible(false);
	waitingScreenTimerText_mc.SetVisible(false);
	boxThingy_mc.SetVisible(false);
	waitingText_txt.SetVisible(false);

	redPotMask_mc.GetPosition(redPotX, redPotY);
	bluePotMask_mc.GetPosition(bluePotX, bluePotY);

	//Snail is currently cooking! The Timer needs to start counting down! Blue Team Timer
	if( blueInt > 0.0 && blueInt < 30.0 )
	{
		cookingTimer_txt.SetString("text", blueCap);
		bluePotMask_mc.GetPosition(bluePotX, bluePotY);
		blueTempValue = (blueTempBarEmpty - blueTempBarFull) * (float(blueInt) / 30.0) + blueTempBarFull;
		bluePotMask_mc.SetPosition(bluePotX, blueTempValue);
	}

	if( (blueInt == 0.0 || redInt > 0.0) )
	{
		UTGRI.blueCookTimer = 0.0;
		bluePotMask_mc.SetPosition(bluePotX, blueTempBarEmpty);
		cookingTimer_txt.SetString("Text", redCap);
	}

	//Snail is currently cooking! The Timer needs to start counting down! Red Team Timer
	if( redInt > 0.0 && redInt < 30.0 )
	{
		cookingTimer_txt.SetString("text", redCap);
		redPotMask_mc.GetPosition(redPotX, redPotY);
		redTempValue = (redTempBarEmpty - redTempBarFull) * (float(redInt) / 30.0) + redTempBarFull;
		redPotMask_mc.SetPosition(redPotX, redTempValue);
	}

	if( (redInt == 0.0 || blueInt > 0.0) )
	{
		UTGRI.redCookTimer = 0.0;
		redPotMask_mc.SetPosition(redPotX, redTempBarEmpty);
		cookingTimer_txt.SetString("text", blueCap);
	}


	//Make sure correct team color is on ammo boxes
	//Also have charger code here
	redIcerAmmo_mc.GetPosition(redIcerX, redIcerY);
	redPepperAmmo_mc.GetPosition(redPepperX, redPepperY);
	blueIcerAmmo_mc.GetPosition(blueIcerX, blueIcerY);
	bluePepperAmmo_mc.GetPosition(bluePepperX, bluePepperY);

	chargeOutline_mc.GetPosition(chargeOutlineX, chargeOutlineY);
	chargeBar_mc.GetPosition(chargeBarX, chargeBarY);

	if (ECGWeap_ASaltRifle(Weapon) != none)
	{
		if( UTP.GetTeamNum() == 0 )
		{
			redPepperAmmo_mc.SetPosition(ammoBoxShowing, redPepperY);

			if( bluePepperX <= ammoBoxShowing )
				bluePepperAmmo_mc.SetPosition(ammoBoxHidden, bluePepperY);

			if( blueIcerY <= ammoBoxShowing )
				blueIcerAmmo_mc.SetPosition(ammoBoxHidden, blueIcerY);

			if( redIcerY <= ammoBoxShowing )
				redIcerAmmo_mc.SetPosition(ammoBoxHidden, redIcerY);
		}

		if( UTP.GetTeamNum() == 1 )
		{
			bluePepperAmmo_mc.SetPosition(ammoBoxShowing, bluePepperY);

			if( redPepperX <= ammoBoxShowing )
				redPepperAmmo_mc.SetPosition(ammoBoxHidden, redPepperY);

			if( blueIcerY <= ammoBoxShowing )
				blueIcerAmmo_mc.SetPosition(ammoBoxHidden, blueIcerY);

			if( redIcerY <= ammoBoxShowing )
				redIcerAmmo_mc.SetPosition(ammoBoxHidden, redIcerY);
		}

		if( chargeOutlineX > chargeOutlineHidden )
			chargeOutline_mc.SetPosition(chargeOutlineHidden, chargeOutlineY);

		if( chargeBarY < chargeBoxEmpty )
			chargeBar_mc.SetPosition(chargeBarX, chargeBoxEmpty);

	}
	else if (ECGWeap_Icer(Weapon) != none)
	{
		if( UTP.GetTeamNum() == 0 )
		{
			redIcerAmmo_mc.SetPosition(ammoBoxShowing, redIcerY);

			if( blueIcerY <= ammoBoxShowing )
				blueIcerAmmo_mc.SetPosition(ammoBoxHidden, blueIcerY);

			if( redPepperX <= ammoBoxShowing )
				redPepperAmmo_mc.SetPosition(ammoBoxHidden, redPepperY);

			if( bluePepperX <= ammoBoxShowing )
				bluePepperAmmo_mc.SetPosition(ammoBoxHidden, bluePepperY);
		}

		if( UTP.GetTeamNum() == 1 )
		{
			blueIcerAmmo_mc.SetPosition(ammoBoxShowing, blueIcerY);

			if( redIcerY <= ammoBoxShowing )
				redIcerAmmo_mc.SetPosition(ammoBoxHidden, redIcerY);

			if( redPepperX <= ammoBoxShowing )
				redPepperAmmo_mc.SetPosition(ammoBoxHidden, redPepperY);

			if( bluePepperX <= ammoBoxShowing )
				bluePepperAmmo_mc.SetPosition(ammoBoxHidden, bluePepperY);
		}

		while( chargeOutlineX < chargeOutlineShowing )
		{
			chargeOutlineX += 3.0;
			chargeOutline_mc.SetPosition(chargeOutlineX, chargeOutlineY);
		}
			
		if( ECGWeap_Icer(Weapon).bIsCharging == true )
		{
			chargeBarTempValue = ( chargeBoxFull - chargeBoxEmpty) * (ECGWeap_Icer(Weapon).chargeAmount / 4.0) + chargeBoxEmpty;
			chargeBar_mc.SetPosition(chargeBarX, chargeBarTempValue);
		}
		else
			chargeBar_mc.SetPosition(chargeBarX, chargeBoxEmpty);
	}

	
	lastAmmoCount = i;
	ammoCurrent_txt.SetString("text", string(i));

	//Updating the Ammo Bar on the Reticule
	ammoBar_mc.GetPosition(ammoBarX, ammoBarY);

	ammoBarValue = (ammoFullLimit - ammoEmptyLimit) * (lastAmmoCount / float(m)) + ammoEmptyLimit;

	ammoBar_mc.SetPosition(ammoBarX, ammoBarValue);

	//Damage Indicator Thingy
	if( getpc2(UTP.Health, UTP.HealthMax) < lastHealthPc )
	{
		damageTimer = 1.0;
	}

	else if( getpc2(UTP.Health, UTP.HealthMax ) > lastHealthPc )
	{
		damageIndicator_mc.SetFloat("alpha", 0.0);
		damageTimer = 0.0;
	}

	if( damageTimer > 0.0 )
	{
		timerValue = damageTimer * 1.0;
		damageIndicator_mc.SetFloat("alpha", FClamp(timerValue, 0.0, 1.0) );
		damageTimer-=(1.0/60.0);
	}
	else if( damageTimer <= 0.0 )
	{
		damageIndicator_mc.SetFloat("alpha", 0.0);
		damageTimer = 0.0;
	}


	//Updating the Health
	lastHealthPc=getpc2(UTP.Health, UTP.HealthMax);

	if( lastHealthPc < 0 )
		lastHealthPc = 0;

	if( lastHealthPc > 100 )
		lastHealthPc = 100;

	healthPoint_txt.SetString("text", string(lastHealthPc));

	//Updating the Health Bar on the Reticule
	healthBar_mc.GetPosition(healthBarX, healthBarY);

	healthBarValue = (healthFullLimit - healthEmptyLimit) * (lastHealthPc / 100.0) + healthEmptyLimit;

	healthBar_mc.SetPosition(healthBarX, healthBarValue);

	hitTimer = ECGPawn(UTP).timeSinceLastHit + 0.5;
	hitTimer = FClamp(hitTimer, 0.0, 1.0);

	reticule_mc.SetFloat("scaleX", hitTimer);
	reticule_mc.SetFloat("scaleY", hitTimer);

	if (hitTimer < 1.0)
	{
		color2.R=1.0;
		colorTransform.multiply=color2;
	}
	else
	{
		color2.R=1.0;
		color2.G=1.0;
		color2.B=1.0;
		colorTransform.multiply=color2;
	}
	reticule_mc.SetColorTransform(colorTransform);
}


DefaultProperties
{
	bDisplayWithHudOff=false;

	healthFullLimit = 317.80;
	healthEmptyLimit = 414.55;

	ammoFullLimit = 317.80;
	ammoEmptyLimit = 415.05;
	
	ammoBoxShowing = 1184.05;
	ammoBoxHidden = 5000.0;

	healthBoxShowing = 89.80;
	healthBoxHidden = -5000.00;

	chargeBoxFull = 331.00;
	chargeBoxEmpty = 396.50;

	chargeOutlineShowing = 666.35;
	chargeOutlineHidden = 628.85;

	blueTempBarEmpty = 99.60;
	blueTempBarFull = -2.40;

	redTempBarEmpty = 99.55;
	redTempBarFull = -1.45;

	damageTimer = 0.0;
	victoryTimer = 3.0;
	waitingScreenTimer = 0.0;

	waitTimer = 15;

	MovieInfo=SwfMovie'ECGHUDswf.ECGHUDswf.ECGHUDswf'
}
