class ECGHUDswf extends UTHUD;

var ECGGFxHUD HudMovie;
var PlayerController PC;
var const Texture2D FlagIconTexture, PotIconTexture, PepperTexture, IcingTexture, RedChefTexture, BlueChefTexture, RedChefCarrying, BlueChefCarrying;
var const Texture2D flagRedIcon, flagBlueIcon, potRedIcon, potBlueIcon;


function DisplayHit(vector HitDir, int Damage, class<DamageType> damageType)
{
}

singular event Destroyed()
{
	if( HudMovie != none )
	{
		HudMovie.Close(true);
		HudMovie=none;
	}
}

simulated function PostBeginPlay()
{
	super.PostBeginPlay();
	HudMovie = new class'ECGGFxHUD';
	HudMovie.PlayerOwner = PC;
	HudMovie.SetTimingMode(TM_Real);
	HudMovie.Init2(HudMovie.PlayerOwner);
}

event PostRender()
{
	super.PostRender();
}

function DrawHUD()
{
	if (UTOwnerPRI != none)
	{
		HudMovie.TickHUD(ECGGameReplicationInfo(WorldInfo.GRI), UTOwnerPRI);
		RenderIcons(ECGGameReplicationInfo(WorldInfo.GRI));
		RenderHUD();
	}
}

function RenderIcons(ECGGameReplicationInfo ECGRI)
{
	local vector iconPosition, objectivePosition, CameraLocation,CameraViewDirection, playerToFlag;
	local CanvasIcon objectiveIcon;
	local int teamNum;
	local Rotator CameraRotation;

	teamNum = ECGRI.snailTeamIndex;

	if (ECGRI.bSnailCooking)
	{
		if(teamNum == 0)
			objectiveIcon = Canvas.MakeIcon(potRedIcon);
		else if(teamNum == 1)
			objectiveIcon = Canvas.MakeIcon(potBlueIcon);
		else
			objectiveIcon = Canvas.MakeIcon(PotIconTexture);

		objectivePosition = ECGRI.snailLocation;
		objectivePosition.Z += 10;
	}
	else if (UTOwnerPRI.bHasFlag)
	{
		if (teamNum == 0)
		{
			objectiveIcon = Canvas.MakeIcon(potRedIcon);
			objectivePosition = ECGRI.redPotLocation;

		}
		else if (teamNum == 1)
		{
			objectiveIcon = Canvas.MakeIcon(potBlueIcon);
			objectivePosition = ECGRI.bluePotLocation;
		}
	}
	else
	{
		if (teamNum == 0)
			objectiveIcon = Canvas.MakeIcon(flagRedIcon);		
		else if (teamNum == 1)
			objectiveIcon = Canvas.MakeIcon(flagBlueIcon);
		else
			objectiveIcon = Canvas.MakeIcon(FlagIconTexture);
		
		objectivePosition = ECGRI.snailLocation;
		objectivePosition.Z += 10;
	}

	PlayerOwner.GetPlayerViewPoint(CameraLocation, CameraRotation);
	CameraViewDirection = Vector(CameraRotation);
	playerToFlag = objectivePosition - UTPawnOwner.Location;
	iconPosition = Canvas.Project(objectivePosition);
	if (playerToFlag dot CameraViewDirection < 0.f)
	{
		iconPosition.Y += 99999;
	}

	iconPosition.X = FClamp(iconPosition.X, -10, Canvas.SizeX - 50);
	iconPosition.Y = FClamp(iconPosition.Y, -10, Canvas.SizeY - 50);

	

	Canvas.DrawIcon(objectiveIcon, iconPosition.X, iconPosition.Y, 0.625);
}

function RenderHUD()
{
	local int teamNum;
	local Vector teamIconPosition;
	local CanvasIcon teamIcon;

	Canvas.SetDrawColor(255, 255, 255);

	teamNum = UTOwnerPRI.GetTeamNum();
	teamIconPosition.X = Canvas.SizeX / 2;
	teamIconPosition.Y = Canvas.SizeY - 200;
	teamIconPosition.Z = 1;
	if (teamNum == 0)
	{
		teamIcon = Canvas.MakeIcon(RedChefTexture);
		if( UTOwnerPRI.bHasFlag )
			teamIcon = Canvas.MakeIcon(RedChefCarrying);

		Canvas.DrawIcon(teamIcon, teamIconPosition.X - teamIcon.UL / 2, teamIconPosition.Y, teamIconPosition.Z);
	}
	else if (teamNum == 1)
	{
		teamIcon = Canvas.MakeIcon(BlueChefTexture);
		if( UTOwnerPRI.bHasFlag )
			teamIcon = Canvas.MakeIcon(BlueChefCarrying);

		Canvas.DrawIcon(teamIcon, teamIconPosition.X - teamIcon.UL / 2, teamIconPosition.Y, teamIconPosition.Z);
	}
}

function TogglePauseMenu()
{
    if ( PauseMenuMovie != none && PauseMenuMovie.bMovieIsOpen )
	{
		if( !WorldInfo.IsPlayInMobilePreview() )
		{
			PauseMenuMovie.PlayCloseAnimation();
		}
		else
		{
			// On mobile previewer, close right away
			CompletePauseMenuClose();
		}
	}
	else
    {
		CloseOtherMenus();

        PlayerOwner.SetPause(True);

        if (PauseMenuMovie == None)
        {
	        PauseMenuMovie = new class'ECG_PauseMenu';
            PauseMenuMovie.MovieInfo = SwfMovie'EscargoMenus.ECG_PauseMenu';
            PauseMenuMovie.bEnableGammaCorrection = FALSE;
			PauseMenuMovie.LocalPlayerOwnerIndex = class'Engine'.static.GetEngine().GamePlayers.Find(LocalPlayer(PlayerOwner.Player));
            PauseMenuMovie.SetTimingMode(TM_Real);
        }

		SetVisible(false);
        PauseMenuMovie.Start();
        PauseMenuMovie.PlayOpenAnimation();

		// Do not prevent 'escape' to unpause if running in mobile previewer
		if( !WorldInfo.IsPlayInMobilePreview() )
		{
			PauseMenuMovie.AddFocusIgnoreKey('Escape');
		}
    }
}

defaultproperties
{
	FlagIconTexture = Texture2D'ECGHUDswf.SnailIcon'
	flagRedIcon = Texture2D'ECGHUDswf.SnailIcons_red'
	flagBlueIcon = Texture2D'ECGHUDswf.SnailIcons_blue'

	PotIconTexture = Texture2D'ECGHUDswf.potIcon'
	potRedIcon = Texture2D'ECGHUDswf.PotIcons_red'
	potBlueIcon = Texture2D'ECGHUDswf.PotIcons_blue'


	RedChefTexture = Texture2D'ECGHUDswf.RedChefTeamIcon'
	BlueChefTexture = Texture2D'ECGHUDswf.BlueChefTeamIcon'
	RedChefCarrying = Texture2D'ECGHUDswf.RedChefTeamIconCarrying'
	BlueChefCarrying = Texture2D'ECGHUDswf.BlueChefTeamIconCarrying'
}